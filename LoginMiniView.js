import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text
} from 'react-native'
 
class LoginMiniView extends Component {
    render() {
        return (
            <View style = {[styles.centerEverything,styles.container]}>
                <Text>
                    LOGIN
                </Text>
                <Text style = {styles.secondText}>
                    OR
                </Text>
                <Text>
                    REGISTER
                </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    centerEverything: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'blue'
      },
      container: {
        flex: 1,
        backgroundColor: 'white',
      },
      secondText:{
          color:'red',
          padding: 20
      }
});

export default LoginMiniView