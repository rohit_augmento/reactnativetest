import React, { Component } from 'react';
import { StyleSheet, Text, View, Picker } from 'react-native';
import SwipeUpDown from 'react-native-swipe-up-down';
import LoginMiniView from './LoginMiniView'

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      animation: "easeInEaseOut"
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to{'\n'}react-native-swipe-up-down</Text>
        <Text style={{ margin: 20 }} onPress={() => this.swipeUpDownRef.showFull()}>
          Tap to open panel
        </Text>
        <Picker
          selectedValue={this.state.animation}
          style={{ width: 100 }}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({animation: itemValue})
          }>
          <Picker.Item label="+91" value="linear"/>
          <Picker.Item label="+1" value="spring"/>
          <Picker.Item label="+2" value="easeInEaseOut"/>
          <Picker.Item label="+4" value="none"/>
        </Picker>
        <SwipeUpDown
          hasRef={ref => (this.swipeUpDownRef = ref)}
          itemMini={
            <LoginMiniView/>
          }
          itemFull={
            <View style={styles.panelContainer}>
              <Text style={styles.instructions}>
                Swipe down to close
              </Text>
            </View>
          }
          onShowMini={() => console.log('mini')}
          onShowFull={() => console.log('full')}
          disablePressToShow={false}
          style={{ backgroundColor: 'white' }}
          swipeHeight={200}
          animation={this.state.animation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  panelContainer: {
    flex: 1,
    justifyContent: 'center'
  },
});